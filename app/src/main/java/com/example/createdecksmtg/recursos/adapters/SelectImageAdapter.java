package com.example.createdecksmtg.recursos.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.createdecksmtg.R;
import com.example.createdecksmtg.recursos.Carta;
import com.example.createdecksmtg.recursos.Deck;
import com.example.createdecksmtg.ui.deck.DeckFragment;

import java.util.ArrayList;

public class SelectImageAdapter extends ArrayAdapter<Carta> {

    public Deck actualDeck;

    public SelectImageAdapter(Context context, int resource, ArrayList<Carta> objects, Deck actualDeck){
        super(context, resource, objects);
        this.actualDeck = actualDeck;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Carta carta = getItem(position);

        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_select_image, parent, false);
        }

        ImageView imgVw_imageLvSelect = convertView.findViewById(R.id.imgVw_imageLvSelect);

        if (imgVw_imageLvSelect!=null){
            Glide.with(getContext())
                    .load(carta.getImage_uris()[4])
                    .into(imgVw_imageLvSelect);
        }

        imgVw_imageLvSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actualDeck.setUrlImage(carta.getImage_uris()[4]);
                DeckFragment.updateDeck(actualDeck);DeckFragment.refreshDecks(getContext());
                DecksListAdapter.dissmissDialaog();
            }
        });



        return convertView;
    }
}
