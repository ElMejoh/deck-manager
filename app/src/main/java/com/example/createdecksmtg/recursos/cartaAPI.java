package com.example.createdecksmtg.recursos;


import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class cartaAPI {

    private String BASE_URL = "http://40.114.237.62:3000/";

    private Carta carta;

    // FORMA LA URL
    public ArrayList<Carta> getCartes() {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("cards")
                .build();
        String url = builtUri.toString();

        return doCall(url);
    }

    // HAVE LA LLAMADA A LA API
    private ArrayList<Carta> doCall(String url) {
        try {
            Log.d("url", url);
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // PROCESA EL JSON DE LA API PARA EL OBJETO
    private ArrayList<Carta> processJson(String jsonResponse) {
        ArrayList<Carta> cartas = new ArrayList<>();
        try {
            JSONArray jsonAllCards = new JSONArray(jsonResponse);
            for (int i = 0; i < jsonAllCards.length(); i++) {

                JSONObject jsonCarta = jsonAllCards.getJSONObject(i);
                Log.d("jsonCarta", jsonCarta.toString());

                carta = new Carta();

                // private String id;
                carta.setId(existInJson("id", jsonCarta));
                Log.d("carta_id", carta.getId());

                // private String name;
                carta.setName(existInJson("name", jsonCarta));
                Log.d("carta_name", carta.getName());

                // private String released_at;
                carta.setReleased_at(existInJson("released_at", jsonCarta));
                Log.d("carta_released_at", carta.getReleased_at());

                // private String[] image_uris;
                if (jsonCarta.has("image_uris")){
                    JSONObject smallImageUrls = jsonCarta.getJSONObject("image_uris");
                    String[] auxImages = getImages(smallImageUrls);
                    carta.setImage_uris(auxImages);
                    Log.d("carta_images_uris", Arrays.toString(carta.getImage_uris()));
                }

                // private String mana_cost;
                carta.setMana_cost(existInJson("mana_cost", jsonCarta));
                Log.d("carta_mana_cost", carta.getMana_cost());

                // private String type_line;
                carta.setType_line(existInJson("type_line", jsonCarta));
                Log.d("carta_type_line", carta.getType_line());

                // private String oracle_text;
                carta.setOracle_text(existInJson("oracle_text", jsonCarta));
                Log.d("carta_oracle_text", carta.getOracle_text());

                // private String power;
                carta.setPower(existInJson("power", jsonCarta));
                Log.d("carta_power", carta.getPower());

                // private String toughness;
                carta.setToughness(existInJson("toughness", jsonCarta));
                Log.d("carta_toughness", carta.getToughness());

                // private ArrayList<String> colors;
                if (jsonCarta.has("colors")){
                    JSONArray arrayColors = jsonCarta.getJSONArray("colors");
                    ArrayList<String> auxColors = new ArrayList<>();

                    for (int j = 0; j < arrayColors.length(); j++) {
                        // R, G, W, U, B
                        auxColors.add(isNull((String) arrayColors.get(j)));
                    }
                    carta.setColors(auxColors);
                    Log.d("carta_colors", carta.getColors().toString());
                }

                // private ArrayList<String> color_identity;
                if (jsonCarta.has("color_identity")){
                    JSONArray arrayColorIdentity = jsonCarta.getJSONArray("color_identity");
                    ArrayList<String> auxColorIdentity = new ArrayList<>();

                    for (int j = 0; j < arrayColorIdentity.length(); j++) {
                        // R, G, W, U, B
                        auxColorIdentity.add(isNull((String) arrayColorIdentity.get(j)));
                    }
                    carta.setColor_identity(auxColorIdentity);
                    Log.d("carta_color_identity", carta.getColor_identity().toString());
                }

                //private String[] legalities;
                if (jsonCarta.has("legalities")){
                    JSONObject objLegalities = jsonCarta.getJSONObject("legalities");
                    String[] auxLegatilities = getLegalities(objLegalities);
                    carta.setLegalities(auxLegatilities);
                    Log.d("carta_legalities", Arrays.toString(carta.getLegalities()));
                }

                // private String set_name;
                carta.setSet_name(existInJson("set_name", jsonCarta));
                Log.d("carta_set_name", carta.getSet_name());

                // private String rarity;
                carta.setRarity(existInJson("rarity", jsonCarta));
                Log.d("carta_rarity", carta.getRarity());


                // private String flavor_text;
                carta.setFlavor_text(existInJson("flavor_text", jsonCarta));
                Log.d("carta_flavor_text", carta.getFlavor_text());

                // private String artist;
                carta.setArtist(existInJson("artist", jsonCarta));
                Log.d("carta_artist", carta.getArtist());

                cartas.add(carta);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return cartas;
    }

    // COGE LAS IMAGENES DE "images_uris"
    private String[] getImages(JSONObject smallImageUrls) throws JSONException {
        String[] auxImg = new String[6];

        // [0] small
        auxImg[0] = existInJson("small", smallImageUrls);
        Log.d("carta_image_small", auxImg[0]);

        // [1] normal
        auxImg[1] = existInJson("normal", smallImageUrls);
        Log.d("carta_image_normal", auxImg[1]);

        // [2] large
        auxImg[2] = existInJson("large", smallImageUrls);
        Log.d("carta_image_large", auxImg[2]);

        // [3] png
        auxImg[3] = existInJson("png", smallImageUrls);
        Log.d("carta_image_png", auxImg[3]);

        // [4] art_crop
        auxImg[4] = existInJson("art_crop", smallImageUrls);
        Log.d("carta_image_art_crop", auxImg[4]);

        // [5] border_crop
        auxImg[5] = existInJson("border_crop", smallImageUrls);
        Log.d("carta_image_border_crop", auxImg[5]);

        return auxImg;
    }

    // COGE LAS LEGALIDADES DE "legalities"
    private String[] getLegalities(JSONObject objLegalities) throws JSONException {
        String[] auxLegal = new String[17];

        // [0] standard
        auxLegal[0] = existInJson("standard", objLegalities);
        Log.d("carta_standard", auxLegal[0]);

        // [1] future
        auxLegal[1] = existInJson("future", objLegalities);
        Log.d("carta_future", auxLegal[1]);


        // [2] historic
        auxLegal[2] = existInJson("historic", objLegalities);
        Log.d("carta_historic", auxLegal[2]);

        // [3] gladiator
        auxLegal[3] = existInJson("gladiator", objLegalities);
        Log.d("carta_gladiator", auxLegal[3]);

        // [4] pioneer
        auxLegal[4] = existInJson("pioneer", objLegalities);
        Log.d("carta_pioneer", auxLegal[4]);

        // [5] modern
        auxLegal[5] = existInJson("modern", objLegalities);
        Log.d("carta_modern", auxLegal[5]);

        // [6] legacy
        auxLegal[6] = existInJson("legacy", objLegalities);
        Log.d("carta_legacy", auxLegal[6]);

        // [7] pauper
        auxLegal[7] = existInJson("pauper", objLegalities);
        Log.d("carta_pauper", auxLegal[7]);

        // [8] vintage
        auxLegal[8] = existInJson("vintage", objLegalities);
        Log.d("carta_vintage", auxLegal[8]);

        // [9] penny
        auxLegal[9] = existInJson("penny", objLegalities);
        Log.d("carta_penny", auxLegal[9]);

        // [10] commander
        auxLegal[10] = existInJson("commander", objLegalities);
        Log.d("carta_commander", auxLegal[10]);

        // [11] brawl
        auxLegal[11] = existInJson("brawl", objLegalities);
        Log.d("carta_brawl", auxLegal[11]);

        // [12] historicbrawl
        auxLegal[12] = existInJson("historicbrawl", objLegalities);
        Log.d("carta_historicbrawl", auxLegal[12]);

        // [13] paupercommander
        auxLegal[13] = existInJson("paupercommander", objLegalities);
        Log.d("carta_paupercommander", auxLegal[13]);

        // [14] duel
        auxLegal[14] = existInJson("duel", objLegalities);
        Log.d("carta_duel", auxLegal[14]);

        // [15] oldschool
        auxLegal[15] = existInJson("oldschool", objLegalities);
        Log.d("carta_oldschool", auxLegal[15]);

        // [16] premodern
        auxLegal[16] = existInJson("premodern", objLegalities);
        Log.d("carta_premodern", auxLegal[16]);


        return auxLegal;
    }

    // COMPRUEBA SI EL VALOR EXISTE DENTRO DEL JSON
    private String existInJson(String auxValueJson, JSONObject jsonCarta) throws JSONException {
        if (jsonCarta.has(auxValueJson)){
            return (isNull(jsonCarta.getString(auxValueJson)));
        }
        else return  "null";
    }

    // COMPRUEBA SI ES EL VALOR ES NULO
    private String isNull(String comprobar){
        if (comprobar.isEmpty()){
            return "null";
        }
        else return comprobar;
    }


}
