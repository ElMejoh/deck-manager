package com.example.createdecksmtg.recursos;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class Carta implements Serializable {

    // ENLACE API : http://40.114.237.62:3000/cards

    private int countDeck;
    private String id;                  // 0000579f-7b35-4ed3-b44c-db2a538066fe
    private String name;                // Fury Sliver
    private String released_at;         // 2006-10-06
    private String[] image_uris;
    // [0] small                        // https://c1.scryfall.com/file/scryfall-cards/small/front/0/0/0000579f-7b35-4ed3-b44c-db2a538066fe.jpg?1562894979
    // [1] normal                       // https://c1.scryfall.com/file/scryfall-cards/normal/front/0/0/0000579f-7b35-4ed3-b44c-db2a538066fe.jpg?1562894979
    // [2] large                        // https://c1.scryfall.com/file/scryfall-cards/large/front/0/0/0000579f-7b35-4ed3-b44c-db2a538066fe.jpg?1562894979
    // [3] png                          // https://c1.scryfall.com/file/scryfall-cards/png/front/0/0/0000579f-7b35-4ed3-b44c-db2a538066fe.png?1562894979
    // [4] art_crop                     // https://c1.scryfall.com/file/scryfall-cards/art_crop/front/0/0/0000579f-7b35-4ed3-b44c-db2a538066fe.jpg?1562894979
    // [5] border_crop                  // https://c1.scryfall.com/file/scryfall-cards/border_crop/front/0/0/0000579f-7b35-4ed3-b44c-db2a538066fe.jpg?1562894979

    private String mana_cost;           // {5}{R}
    private String type_line;           // Creature — Sliver
    private String oracle_text;         // All Sliver creatures have double strike.
    private String power;               // 3
    private String toughness;           // 3
    private ArrayList<String> colors;   // R
    // [R] Red
    // [U] Blue
    // [B] Black
    // [G] Green
    // [W] White

    private ArrayList<String> color_identity; // U
    // [R] Red
    // [U] Blue
    // [B] Black
    // [G] Green
    // [W] White

    private String[] legalities;
    // [0] standard                     // not_legal
    // [1] future                       // not_legal
    // [2] historic                     // not_legal
    // [3] gladiator                    // not_legal
    // [4] pioneer                      // not_legal
    // [5] modern                       // legal
    // [6] legacy                       // legal
    // [7] pauper                       // not_legal
    // [8] vintage                      // legal
    // [9] penny                        // legal
    // [10] commander                   // legal
    // [11] brawl                       // not_legal
    // [12] historicbrawl               // not_legal
    // [13] paupercommander             // restricted
    // [14] duel                        // legal
    // [15] oldschool                   // not_legal
    // [16] premodern                   // not_legal

    private String set_name;            // Time Spiral
    private String rarity;              // uncommon
    private String flavor_text;         // \"A rift opened, and our arrows were abruptly stilled. To move was to push the world. But the sliver's claw still twitched, red wounds appeared in Thed's chest, and ribbons of blood hung in the air.\"\n—Adom Capashen, Benalish hero
    private String artist;              // Paolo Parente


    // GETTERS
    public int getCountDeck() { return countDeck; }
    public String getId() { return id; }
    public String getName() { return name; }
    public String getReleased_at() { return released_at; }
    public String[] getImage_uris() { return image_uris; }
    public String getMana_cost() { return mana_cost; }
    public String getType_line() { return type_line; }
    public String getOracle_text() { return oracle_text; }
    public String getPower() { return power; }
    public String getToughness() { return toughness; }
    public ArrayList<String> getColors() { return colors; }
    public ArrayList<String> getColor_identity() { return color_identity; }
    public String[] getLegalities() { return legalities; }
    public String getSet_name() { return set_name; }
    public String getRarity() { return rarity; }
    public String getFlavor_text() { return flavor_text; }
    public String getArtist() { return artist; }

    // SETTERS
    public void setCountDeck(int countDeck) { this.countDeck = countDeck; }
    public void setId(String id) { this.id = id; }
    public void setName(String name) { this.name = name; }
    public void setReleased_at(String released_at) { this.released_at = released_at; }
    public void setImage_uris(String[] image_uris) { this.image_uris = image_uris; }
    public void setMana_cost(String mana_cost) { this.mana_cost = mana_cost; }
    public void setType_line(String type_line) { this.type_line = type_line; }
    public void setOracle_text(String oracle_text) { this.oracle_text = oracle_text; }
    public void setPower(String power) { this.power = power; }
    public void setToughness(String toughness) { this.toughness = toughness; }
    public void setColors(ArrayList<String> colors) { this.colors = colors; }
    public void setColor_identity(ArrayList<String> color_identity) { this.color_identity = color_identity; }
    public void setLegalities(String[] legalities) { this.legalities = legalities; }
    public void setSet_name(String set_name) { this.set_name = set_name; }
    public void setRarity(String rarity) { this.rarity = rarity; }
    public void setFlavor_text(String flavor_text) { this.flavor_text = flavor_text; }
    public void setArtist(String artist) { this.artist = artist; }


    @Override
    public String toString() {
        return "Carta{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", released_at='" + released_at + '\'' +
                ", image_uris=" + Arrays.toString(image_uris) +
                ", mana_cost='" + mana_cost + '\'' +
                ", type_line='" + type_line + '\'' +
                ", oracle_text='" + oracle_text + '\'' +
                ", power='" + power + '\'' +
                ", toughness='" + toughness + '\'' +
                ", colors=" + colors +
                ", color_identity=" + color_identity +
                ", legalities=" + Arrays.toString(legalities) +
                ", set_name='" + set_name + '\'' +
                ", rarity='" + rarity + '\'' +
                ", flavor_text='" + flavor_text + '\'' +
                ", artist='" + artist + '\'' +
                '}';
    }
}
