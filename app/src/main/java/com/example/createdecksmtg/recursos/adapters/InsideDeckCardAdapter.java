package com.example.createdecksmtg.recursos.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.createdecksmtg.DetailsActivity;
import com.example.createdecksmtg.R;
import com.example.createdecksmtg.recursos.Carta;
import com.example.createdecksmtg.recursos.Deck;
import com.example.createdecksmtg.ui.insidedeck.InsideDeckFragment;

import java.util.ArrayList;

public class InsideDeckCardAdapter extends ArrayAdapter<Carta> {

    public Deck deck;


    public InsideDeckCardAdapter(Context context, int resource, ArrayList<Carta> objects, Deck deck){
        super(context, resource, objects);
        this.deck = deck;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Carta carta = getItem(position);

        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_card_in_deck, parent, false);
        }

        ImageView imgVw_cardImageEachInDeck = convertView.findViewById(R.id.imgVw_cardImageEachInDeck);
        ImageView imgVw_deleteEachCardInDeck = convertView.findViewById(R.id.imgVw_deleteEachCardInDeck);

        TextView txtVw_countCardEachInDeck = convertView.findViewById(R.id.txtVw_countCardEachInDeck);
        TextView txtVw_nameCardEachInDeck = convertView.findViewById(R.id.txtVw_nameCardEachInDeck);
        TextView txtVw_typeCardEachInDeck = convertView.findViewById(R.id.txtVw_typeCardEachInDeck);
        TextView txtVw_manaCostCardEachInDeck = convertView.findViewById(R.id.txtVw_manaCostCardEachInDeck);

        for (int i = 0; i < deck.getAllCartas().size(); i++) {
            if (deck.getAllCartas().get(i).getId().equals(carta.getId())){
                txtVw_countCardEachInDeck.setText(String.valueOf(deck.getAllCartas().get(i).getCountDeck()));
            }
        }
        txtVw_nameCardEachInDeck.setText(carta.getName());
        txtVw_typeCardEachInDeck.setText(carta.getType_line());
        txtVw_manaCostCardEachInDeck.setText(carta.getMana_cost());

        if (!carta.getImage_uris()[4].equals("null")){
            Glide.with(getContext())
                    .load(carta.getImage_uris()[4])
                    .into(imgVw_cardImageEachInDeck);
        }

        if (carta.getMana_cost().equals("null")){
            txtVw_manaCostCardEachInDeck.setVisibility(convertView.GONE);
        }

        // ELIMINA UNA CARTA DEL DECK
        imgVw_deleteEachCardInDeck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < deck.getAllCartas().size(); i++) {
                    if (deck.getAllCartas().get(i).getId().equals(carta.getId())){
                        deck.getAllCartas().remove(i);
                    }
                }
                if (carta.getType_line().contains("Creature")) {
                    deck.setCreatures(deleteCartaType(deck.getCreatures(), carta));
                }
                else if (carta.getType_line().contains("Planeswalker")) {
                    deck.setPlainswalkers(deleteCartaType(deck.getPlainswalkers(), carta));
                }
                else if (carta.getType_line().contains("Instant")) {
                    deck.setInstant(deleteCartaType(deck.getInstant(), carta));
                }
                else if (carta.getType_line().contains("Sorcery")) {
                    deck.setSorcery(deleteCartaType(deck.getSorcery(), carta));
                }
                else if (carta.getType_line().contains("Artifact")) {
                    deck.setArtifact(deleteCartaType(deck.getArtifact(), carta));
                }
                else if (carta.getType_line().contains("Enchantment")) {
                    deck.setEnchantments(deleteCartaType(deck.getEnchantments(), carta));
                }
                else if (carta.getType_line().contains("Land")) {
                    deck.setLands(deleteCartaType(deck.getLands(), carta));
                }
                InsideDeckFragment.setDeck(deck, getContext());
            }
        });

        // DETAILS
        imgVw_cardImageEachInDeck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), DetailsActivity.class);
                intent.putExtra("carta", carta);
                getContext().startActivity(intent);
                InsideDeckFragment.updateInDeck(getContext());
            }
        });

        return convertView;
    }

    public ArrayList<Carta> deleteCartaType(ArrayList<Carta> typeArray, Carta carta){
        for (int i = 0; i < typeArray.size(); i++) {
            if (typeArray.get(i).getId().equals(carta.getId())){
                typeArray.remove(i);
            }
        }
        return typeArray;
    }
}
