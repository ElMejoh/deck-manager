package com.example.createdecksmtg.recursos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class Deck implements Serializable {

    public String name;
    public Date created_at;
    public String urlImage;
    public ArrayList<Carta> allCartas;
    public ArrayList<Carta> creatures;
    public ArrayList<Carta> plainswalkers;
    public ArrayList<Carta> instant;
    public ArrayList<Carta> sorcery;
    public ArrayList<Carta> artifact;
    public ArrayList<Carta> enchantments;
    public ArrayList<Carta> lands;
    public ArrayList<Carta> tokens;

    // CONSTRUCTOR
    public Deck(String name, Date created_at, String urlImage){
        this.name = name;
        this.created_at = created_at;
        this.urlImage = urlImage;
        this.allCartas = new ArrayList<>();
        this.creatures = new ArrayList<>();
        this. plainswalkers = new ArrayList<>();
        this.instant = new ArrayList<>();
        this.sorcery = new ArrayList<>();
        this.artifact = new ArrayList<>();
        this.enchantments = new ArrayList<>();
        this.lands = new ArrayList<>();
        this.tokens = new ArrayList<>();
    }

    // GETTER
    public String getName() { return name; }
    public Date getCreated_at() { return created_at; }
    public String getUrlImage() { return urlImage; }
    public ArrayList<Carta> getAllCartas() { return allCartas; }
    public ArrayList<Carta> getCreatures() { return creatures; }
    public ArrayList<Carta> getPlainswalkers() { return plainswalkers; }
    public ArrayList<Carta> getInstant() { return instant; }
    public ArrayList<Carta> getSorcery() { return sorcery; }
    public ArrayList<Carta> getArtifact() { return artifact; }
    public ArrayList<Carta> getEnchantments() { return enchantments; }
    public ArrayList<Carta> getLands() { return lands; }
    public ArrayList<Carta> getTokens() { return tokens; }

    // SETTER
    public void setName(String name) { this.name = name; }
    public void setCreated_at(Date created_at) { this.created_at = created_at; }
    public void setCreatures(ArrayList<Carta> creatures) { this.creatures = creatures; }
    public void setUrlImage(String urlImage) { this.urlImage = urlImage; }
    public void setAllCartas(ArrayList<Carta> allCartas) { this.allCartas = allCartas; }
    public void setPlainswalkers(ArrayList<Carta> plainswalkers) { this.plainswalkers = plainswalkers; }
    public void setInstant(ArrayList<Carta> instant) { this.instant = instant; }
    public void setSorcery(ArrayList<Carta> sorcery) { this.sorcery = sorcery; }
    public void setArtifact(ArrayList<Carta> artifact) { this.artifact = artifact; }
    public void setEnchantments(ArrayList<Carta> enchantments) { this.enchantments = enchantments; }
    public void setLands(ArrayList<Carta> lands) { this.lands = lands; }
    public void setTokens(ArrayList<Carta> tokens) { this.tokens = tokens; }

    // ADD CARTA
    public void addCarta(Carta carta){
        boolean repeat = false;
        for (int i = 0; i < allCartas.size(); i++) {
            if (carta.getId().equals(allCartas.get(i).getId())){
                repeat = true;
                allCartas.get(i).setCountDeck(allCartas.get(i).getCountDeck()+1);
            }
        }
        if (!repeat){
            carta.setCountDeck(1);
            allCartas.add(carta);
            if (carta.getType_line().contains("Creature")) creatures.add(carta);
            else if (carta.getType_line().contains("Planeswalker")) plainswalkers.add(carta);
            else if (carta.getType_line().contains("Instant")) instant.add(carta);
            else if (carta.getType_line().contains("Sorcery")) sorcery.add(carta);
            else if (carta.getType_line().contains("Artifact")) artifact.add(carta);
            else if (carta.getType_line().contains("Enchantment")) enchantments.add(carta);
            else if (carta.getType_line().contains("Land")) lands.add(carta);
        }
    }

    // toString
    @Override
    public String toString() {
        return "Deck{" +
                "name='" + name + '\'' +
                ", created_at=" + created_at +
                ", urlImage=" + urlImage +
                ", allCartas=" + allCartas +
                ", creatures=" + creatures +
                ", plainswalkers=" + plainswalkers +
                ", instant=" + instant +
                ", sorcery=" + sorcery +
                ", artifact=" + artifact +
                ", enchantments=" + enchantments +
                ", lands=" + lands +
                ", tokens=" + tokens +
                '}';
    }
}
