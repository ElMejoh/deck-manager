package com.example.createdecksmtg.recursos.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.createdecksmtg.DetailsActivity;
import com.example.createdecksmtg.R;
import com.example.createdecksmtg.recursos.Carta;
import com.example.createdecksmtg.ui.inventory.InventoryFragment;


import java.util.ArrayList;

public class AddCardAdapter extends ArrayAdapter<Carta> {

    public AddCardAdapter(Context context, int resource, ArrayList<Carta> objects){
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Carta carta = getItem(position);

        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_card_add, parent, false);
        }

        TextView txtVw_nameCardAdd = convertView.findViewById(R.id.txtVw_nameCardAdd);
        ImageButton imgBtn_addCardInventory = convertView.findViewById(R.id.imgBtn_addCardInventory);


        imgBtn_addCardInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean repeats = false;
                ArrayList<Carta> cartasInventory = InventoryFragment.getCartasInventory();
                for (int i = 0; i < cartasInventory.size(); i++) {
                    if (carta.getId().equals(cartasInventory.get(i).getId())){
                        repeats = true;
                    }
                }
                if (!repeats) InventoryFragment.addCartaInventory(carta);
                InventoryFragment.refreshInventory(getContext());
            }
        });

        txtVw_nameCardAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), DetailsActivity.class);
                intent.putExtra("carta", carta);
                getContext().startActivity(intent);
            }
        });

        txtVw_nameCardAdd.setText(carta.getName());

        return convertView;
    }
}
