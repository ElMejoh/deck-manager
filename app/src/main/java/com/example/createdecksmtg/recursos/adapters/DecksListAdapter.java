package com.example.createdecksmtg.recursos.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.createdecksmtg.DetailsActivity;
import com.example.createdecksmtg.InsideDeckActivity;
import com.example.createdecksmtg.R;
import com.example.createdecksmtg.recursos.Deck;
import com.example.createdecksmtg.ui.deck.DeckFragment;
import com.example.createdecksmtg.ui.insidedeck.InsideDeckFragment;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;

public class DecksListAdapter extends ArrayAdapter<Deck> {

    public ArrayList<Deck> allDecks;

    public static AlertDialog dialog2;

    public DecksListAdapter(Context context, int resource, ArrayList<Deck> objects){
        super(context, resource, objects);
        this.allDecks = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Deck deck = getItem(position);

        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_each_deck, parent, false);
        }

        ImageView imgVw_deckImg = convertView.findViewById(R.id.imgVw_deckImg);
        TextView txtVw_nameDeck = convertView.findViewById(R.id.txtVw_nameDeck);
        TextView txtVw_cardCount = convertView.findViewById(R.id.txtVw_cardCount);
        ImageView imgVw_optionsMenu = convertView.findViewById(R.id.imgVw_optionsMenu);

        Log.d("Name_deck", deck.getName());

        txtVw_nameDeck.setText(deck.getName());

        int count = 0;
        for (int i = 0; i < deck.getAllCartas().size(); i++) {
            count = count + deck.getAllCartas().get(i).getCountDeck();
        }
        txtVw_cardCount.setText(String.valueOf(count));

        Glide.with(getContext())
                .load(deck.getUrlImage())
                .into(imgVw_deckImg);

        imgVw_deckImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), InsideDeckActivity.class);
                intent.putExtra("deck", deck);
                getContext().startActivity(intent);
            }
        });

        imgVw_optionsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());

                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                final View optionsDeck = inflater.inflate(R.layout.popup_options_deck, null);

                TextView txtVw_changeDeckName = optionsDeck.findViewById(R.id.txtVw_changeDeckName);
                TextView txtVw_changeDeckImage = optionsDeck.findViewById(R.id.txtVw_changeDeckImage);
                TextView txtVw_deleteDeckOptions = optionsDeck.findViewById(R.id.txtVw_deleteDeckOptions);
                Button btn_cancelOptionsDeck = optionsDeck.findViewById(R.id.btn_cancelOptionsDeck);

                dialogBuilder.setView(optionsDeck);
                AlertDialog dialog = dialogBuilder.show();
                dialog.show();

                // CHANGE NAME
                txtVw_changeDeckName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());

                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                        final View selectImage = inflater.inflate(R.layout.popup_name_new_deck, null);

                        TextInputEditText txtInpEd_newDeck = selectImage.findViewById(R.id.txtInpEd_newDeck);
                        Button btn_cancelNewDeck = selectImage.findViewById(R.id.btn_cancelNewDeck);
                        ImageView imgBtn_newDeck = selectImage.findViewById(R.id.imgBtn_newDeck);

                        txtInpEd_newDeck.setText(String.valueOf(deck.getName()));

                        dialogBuilder.setView(selectImage);
                        AlertDialog dialog = dialogBuilder.show();
                        dialog.show();

                        // CANCEL BUTTON
                        btn_cancelNewDeck.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });

                        // CHANGE NAME
                        imgBtn_newDeck.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                deck.setName(String.valueOf(txtInpEd_newDeck.getText()));
                                dialog.dismiss();
                                DeckFragment.updateDeck(deck);
                            }
                        });


                    }
                });

                // CHANGE IMAGE
                txtVw_changeDeckImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());

                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                        final View selectImage = inflater.inflate(R.layout.popup_select_image, null);

                        ListView lsVw_imageList = selectImage.findViewById(R.id.lsVw_imageList);
                        Button btn_cancelImageSelect = selectImage.findViewById(R.id.btn_cancelImageSelect);

                        SelectImageAdapter imageViewArrayAdapter = new SelectImageAdapter(
                                getContext(),
                                R.layout.lv_select_image,
                                deck.getAllCartas(),
                                deck
                        );
                        lsVw_imageList.setAdapter(imageViewArrayAdapter);

                        dialogBuilder.setView(selectImage);
                        dialog2 = dialogBuilder.show();
                        dialog2.show();

                        btn_cancelImageSelect.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog2.dismiss();
                            }
                        });

                    }
                });

                // DELETE DECK
                txtVw_deleteDeckOptions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        for (int i = 0; i < allDecks.size(); i++) {
                            if (allDecks.get(i).getCreated_at().equals(deck.getCreated_at())){
                                allDecks.remove(i);
                            }
                        }
                        DeckFragment.setAllDecks(allDecks);
                        dialog.dismiss();
                        DeckFragment.refreshDecks(getContext());
                    }
                });

                // CANCEL OPTIONS
                btn_cancelOptionsDeck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            }
        });

        return convertView;
    }

    public static void dissmissDialaog () {
        dialog2.dismiss();
    }
}
