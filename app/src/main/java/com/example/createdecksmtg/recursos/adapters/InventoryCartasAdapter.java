package com.example.createdecksmtg.recursos.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.createdecksmtg.DetailsActivity;
import com.example.createdecksmtg.R;
import com.example.createdecksmtg.recursos.Carta;
import com.example.createdecksmtg.recursos.Deck;
import com.example.createdecksmtg.ui.insidedeck.InsideDeckFragment;
import com.example.createdecksmtg.ui.inventory.InventoryFragment;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;

public class InventoryCartasAdapter extends ArrayAdapter<Carta> {

    public String action;
    public Deck deck;
    public int howMany;

    public InventoryCartasAdapter(Context context, int resource, ArrayList<Carta> objects, String action, Deck deck){
        super(context, resource, objects);
        this.action = action;
        this.deck = deck;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Carta carta = getItem(position);

        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_card_inventory, parent, false);
        }

        ImageView imgVw_cardImageInventory = convertView.findViewById(R.id.imgVw_cardImageInventory);
        TextView txtVw_cardNameInventory = convertView.findViewById(R.id.txtVw_cardNameInventory);
        TextView txtVw_typeCardInventory = convertView.findViewById(R.id.txtVw_typeCardInventory);
        ImageView imgVw_actionCardInventory = convertView.findViewById(R.id.imgVw_actionCardInventory);

        txtVw_cardNameInventory.setText(carta.getName());
        txtVw_typeCardInventory.setText(carta.getType_line());
        Glide.with(getContext())
                .load(carta.getImage_uris()[0])
                .into(imgVw_cardImageInventory);

        if (action.equals("Add")){
            imgVw_actionCardInventory.setImageResource(R.drawable.ic_add_card);
            View finalConvertView = convertView;

            imgVw_actionCardInventory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    howMany = 1;

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(InsideDeckFragment.getBinding().getRoot().getContext());


                    LayoutInflater inflater = (LayoutInflater) InsideDeckFragment.getBinding().getRoot().getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                    final View addHowMany = inflater.inflate(R.layout.popup_add_how_many, null);

                    TextInputEditText txtInpEd_numberAddCards = addHowMany.findViewById(R.id.txtInpEd_numberAddCards);
                    ImageButton imgBtn_addOneCard = addHowMany.findViewById(R.id.imgBtn_addOneCard);
                    ImageButton imgBtn_deleteOneCard = addHowMany.findViewById(R.id.imgBtn_deleteOneCard);
                    Button btn_addHowManyCards = addHowMany.findViewById(R.id.btn_addHowManyCards);

                    txtInpEd_numberAddCards.setText(String.valueOf(howMany));

                    dialogBuilder.setView(addHowMany);
                    AlertDialog dialog = dialogBuilder.show();
                    dialog.show();

                    imgBtn_addOneCard.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            howMany = howMany + 1;
                            txtInpEd_numberAddCards.setText(String.valueOf(howMany));
                        }
                    });

                    imgBtn_deleteOneCard.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (howMany > 1) {
                                howMany = howMany - 1;
                                txtInpEd_numberAddCards.setText(String.valueOf(howMany));
                            }
                        }
                    });

                    btn_addHowManyCards.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            howMany = Integer.parseInt(String.valueOf(txtInpEd_numberAddCards.getText()));
                            Log.e("Valor_How", String.valueOf(howMany));
                            for (int i = 0; i < howMany; i++) {
                                deck.addCarta(carta);
                            }
                            InsideDeckFragment.setDeck(deck, getContext());
                            InsideDeckFragment.updateInDeck(getContext());
                            dialog.dismiss();
                        }
                    });
                }
            });

        }
        else if (action.equals("Delete")){
            imgVw_actionCardInventory.setImageResource(R.drawable.ic_delete_card);
            imgVw_actionCardInventory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ArrayList<Carta> cartasInventory = InventoryFragment.getCartasInventory();
                    for (int i = 0; i < cartasInventory.size(); i++) {
                        if (carta.getId().equals(cartasInventory.get(i).getId())){
                            cartasInventory.remove(i);
                        }
                    }
                    InventoryFragment.refreshInventory(getContext());
                }
            });
        }



        imgVw_cardImageInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), DetailsActivity.class);
                intent.putExtra("carta", carta);
                getContext().startActivity(intent);
            }
        });

        return convertView;
    }
}

