package com.example.createdecksmtg.ui.insidedeck;

import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.createdecksmtg.DeckActivity;
import com.example.createdecksmtg.R;
import com.example.createdecksmtg.databinding.InsideDeckFragmentBinding;
import com.example.createdecksmtg.recursos.Carta;
import com.example.createdecksmtg.recursos.Deck;
import com.example.createdecksmtg.recursos.adapters.InsideDeckCardAdapter;
import com.example.createdecksmtg.recursos.adapters.InventoryCartasAdapter;
import com.example.createdecksmtg.ui.deck.DeckFragment;
import com.example.createdecksmtg.ui.inventory.InventoryFragment;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;

public class InsideDeckFragment extends Fragment {

    private InsideDeckViewModel mViewModel;

    @NonNull public static InsideDeckFragmentBinding binding;

    public static InsideDeckCardAdapter insideDeckCardAdapter;

    public static Deck deck;

    public static AlertDialog.Builder dialogBuilder;
    public static AlertDialog dialog;

    public static InsideDeckFragment newInstance() { return new InsideDeckFragment(); }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inside_deck_fragment, container, false);

        binding = InsideDeckFragmentBinding.bind(view);

        Intent i = getActivity().getIntent();

        binding.imgVwReturnArrowEachDeck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), DeckActivity.class);
                startActivity(intent);
                updateInDeck(getContext());
            }
        });

        if (i != null) {
            deck = (Deck) i.getSerializableExtra("deck");

            if (deck != null) {
                updateInDeck(getContext());
            }
        }

        binding.fltActBtnAddCartaInDeck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewCard(deck, InventoryFragment.getCartasInventory());
                updateInDeck(getContext());
            }
        });

        return view;
    }

    public static void updateInDeck(Context context) {
        binding.txtVwNameEachDeck.setText(deck.getName());

        // CRIATURAS
        insideDeckCardAdapter = new InsideDeckCardAdapter(
                context,
                R.layout.lv_card_in_deck,
                deck.getCreatures(),
                deck
        );
        binding.lsVwCreaturesEachDeck.setAdapter(insideDeckCardAdapter);
        comprobarTamaño(deck.getCreatures(), binding.lsVwCreaturesEachDeck);
        deleteOnEmpty(binding.lsVwCreaturesEachDeck, deck.getCreatures(), binding.txtVwCreatureTypeEachDeck);

        // PLANESWALKER
        insideDeckCardAdapter = new InsideDeckCardAdapter(
                context,
                R.layout.lv_card_in_deck,
                deck.getPlainswalkers(),
                deck
        );
        binding.lsVwPlaneswalkerEachDeck.setAdapter(insideDeckCardAdapter);
        comprobarTamaño(deck.getPlainswalkers(), binding.lsVwPlaneswalkerEachDeck);
        deleteOnEmpty(binding.lsVwPlaneswalkerEachDeck, deck.getPlainswalkers(), binding.txtVwPlaneswalkersTypeEachDeck);

        // INSTANT
        insideDeckCardAdapter = new InsideDeckCardAdapter(
                context,
                R.layout.lv_card_in_deck,
                deck.getInstant(),
                deck
        );
        binding.lsVwInstantEachDeck.setAdapter(insideDeckCardAdapter);
        comprobarTamaño(deck.getInstant(), binding.lsVwInstantEachDeck);
        deleteOnEmpty(binding.lsVwInstantEachDeck, deck.getInstant(), binding.txtVwInstantTypeEachDeck);

        // SORCERY
        insideDeckCardAdapter = new InsideDeckCardAdapter(
                context,
                R.layout.lv_card_in_deck,
                deck.getSorcery(),
                deck
        );
        binding.lsVwSorceryEachDeck.setAdapter(insideDeckCardAdapter);
        comprobarTamaño(deck.getSorcery(), binding.lsVwSorceryEachDeck);
        deleteOnEmpty(binding.lsVwSorceryEachDeck, deck.getSorcery(), binding.txtVwSorceryTypeEachDeck);

        // ARTIFACT
        insideDeckCardAdapter = new InsideDeckCardAdapter(
                context,
                R.layout.lv_card_in_deck,
                deck.getArtifact(),
                deck
        );
        binding.lsVwArtifactEachDeck.setAdapter(insideDeckCardAdapter);
        comprobarTamaño(deck.getArtifact(), binding.lsVwArtifactEachDeck);
        deleteOnEmpty(binding.lsVwArtifactEachDeck, deck.getArtifact(), binding.txtVwArtifactTypeEachDeck);

        // ENCHANTMETN
        insideDeckCardAdapter = new InsideDeckCardAdapter(
                context,
                R.layout.lv_card_in_deck,
                deck.getEnchantments(),
                deck
        );
        binding.lsVwEnchantmentEachDeck.setAdapter(insideDeckCardAdapter);
        comprobarTamaño(deck.getEnchantments(), binding.lsVwEnchantmentEachDeck);
        deleteOnEmpty(binding.lsVwEnchantmentEachDeck, deck.getEnchantments(), binding.txtVwEnchantmentTypeEachDeck);

        // LANDS
        insideDeckCardAdapter = new InsideDeckCardAdapter(
                context,
                R.layout.lv_card_in_deck,
                deck.getLands(),
                deck
        );
        binding.lsVwLandsEachDeck.setAdapter(insideDeckCardAdapter);
        comprobarTamaño(deck.getLands(), binding.lsVwLandsEachDeck);
        deleteOnEmpty(binding.lsVwLandsEachDeck, deck.getLands(), binding.txtVwLandsTypeEachDeck);

        DeckFragment.updateDeck(deck);

    }


    private static void comprobarTamaño(ArrayList<Carta> creatures, ListView lsVwCreaturesEachDeck) {
        ViewGroup.LayoutParams params = lsVwCreaturesEachDeck.getLayoutParams();
        int cardHeigh = 210;
        int listHeigh = 0;
        for (int i = 0; i < creatures.size()+1; i++) {
            listHeigh = listHeigh + cardHeigh;
        }
        params.height = listHeigh;
        lsVwCreaturesEachDeck.setLayoutParams(params);
    }

    private static void deleteOnEmpty(ListView lsVw, ArrayList<Carta> array, TextView text) {
        if (array.isEmpty()){
            text.setVisibility(binding.getRoot().GONE);
            lsVw.setVisibility(binding.getRoot().GONE);
        }
    }

    private void addNewCard(Deck deck, ArrayList<Carta> inventory) {
        dialogBuilder = new AlertDialog.Builder(binding.getRoot().getContext());

        final View contactPopUpView = getLayoutInflater().inflate(R.layout.popup_add_card, null);

        // CONTACT POP UP
        ListView lsVw_cartasAdd = contactPopUpView.findViewById(R.id.lsVw_cartasAdd);
        ImageView imgVw_searchAddCard = contactPopUpView.findViewById(R.id.imgVw_searchAddCard);
        Button btn_cancelAdd = contactPopUpView.findViewById(R.id.btn_cancelAdd);
        TextInputEditText txtInpEd_searchCardsAdd = contactPopUpView.findViewById(R.id.txtInpEd_searchCardsAdd);

        InventoryCartasAdapter inventoryCartasAdapter = new InventoryCartasAdapter(
                binding.getRoot().getContext(),
                R.layout.lv_card_inventory,
                inventory,
                "Add",
                deck
        );
        lsVw_cartasAdd.setAdapter(inventoryCartasAdapter);

        // PUP UP DIALOG BUILDER
        dialogBuilder.setView(contactPopUpView);
        dialog = dialogBuilder.show();
        dialog.show();


        btn_cancelAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                updateInDeck(binding.getRoot().getContext());
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(InsideDeckViewModel.class);
        // TODO: Use the ViewModel
    }

    // GETTER + SETTER - DECK
    public static Deck getDeck() { return deck; }
    public static void setDeck(Deck deck, Context context) {
        InsideDeckFragment.deck = deck;
        updateInDeck(context);
    }

    @NonNull
    public static InsideDeckFragmentBinding getBinding() {
        return binding;
    }
}