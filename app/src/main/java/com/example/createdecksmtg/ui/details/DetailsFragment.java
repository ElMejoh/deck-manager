package com.example.createdecksmtg.ui.details;

import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.renderscript.ScriptGroup;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.createdecksmtg.R;
import com.example.createdecksmtg.databinding.DetailFragmentBinding;
import com.example.createdecksmtg.recursos.Carta;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class DetailsFragment extends Fragment {

    private DetailsViewModel mViewModel;

    @NonNull public DetailFragmentBinding binding;

    public static DetailsFragment newInstance() {
        return new DetailsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_fragment, container, false);

        binding = DetailFragmentBinding.bind(view);

        Intent i = getActivity().getIntent();

        if (i != null) {
            Carta carta = (Carta) i.getSerializableExtra("carta");

            onClickImagePopUp(carta, view);

            if (carta != null) {
                updateDetails(carta);
            }
        }

        return view;
    }

    private void onClickImagePopUp(Carta carta, View view) {
        ImageView imgVw_topCardDetails = view.findViewById(R.id.imgVw_topCardDetails);
        imgVw_topCardDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final View popUpView = getLayoutInflater().inflate(R.layout.popup_image, null);

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(view.getContext());

                ImageView imgVw_popupImage = popUpView.findViewById(R.id.imgVw_popupImage);
                Glide.with(view.getContext())
                        .load(carta.getImage_uris()[1])
                        .into(imgVw_popupImage);

                // PUP UP DIALOG BUILDER
                dialogBuilder.setView(popUpView);
                AlertDialog dialog = dialogBuilder.show();
                dialog.show();
            }
        });
    }


    // Pone la información de la carta en el Details
    private void updateDetails(Carta carta) {

        // TOP - ImageView
        Glide.with(getContext())
                .load(carta.getImage_uris()[4])
                .into(binding.imgVwTopCardDetails);

        // TOP - TextView
        // Card Name
        binding.txtVwCardNameDetail.setText(carta.getName());
        // Type Line
        binding.txtVwCardTypeDetail.setText(carta.getType_line());
        // Power / Toughness
        if (carta.getPower().equals("null") && carta.getToughness().equals("null")) binding.txtVwPowerToughnessDetail.setVisibility(binding.getRoot().GONE);
        else {
            binding.txtVwPowerToughnessDetail.setText( new StringBuilder()
                    .append(carta.getPower())
                    .append("/")
                    .append(carta.getToughness())
                    .toString());
        }
        // Rarity
        comprobarRareza(carta.getRarity(), binding.txtVwRarityDetails);
        // Mana Cost
        if (carta.getMana_cost().equals("null")) binding.txtVwManaCost.setVisibility(binding.getRoot().GONE);
        else binding.txtVwManaCost.setText(carta.getMana_cost());


        // MIDDLE - Long Texts
        // Oracle Text
        if (carta.getOracle_text().equals("null")) binding.txtVwOracleTextDetails.setVisibility(binding.getRoot().GONE);
        else binding.txtVwOracleTextDetails.setText(carta.getOracle_text());
        // Flavor Text
        if (carta.getFlavor_text().equals("null")) binding.txtVwFlavorTextDetail.setVisibility(binding.getRoot().GONE);
        else binding.txtVwFlavorTextDetail.setText(carta.getFlavor_text());

        // MIDDLE - Additional Info
        // Release Date
        binding.txtVwDateReleasedDetails.setText(carta.getReleased_at());
        // Artist
        binding.txtVwArtistNameDetails.setText(carta.getArtist());
        // Set Name
        binding.txtVwSetNameDetails.setText(carta.getSet_name());


        // BOTTOM - TextView

        // LINE - 1
        // Standard
        comprobarLelagidad(binding.txtVwStandardLegalitie, carta.getLegalities()[0]);
        // Future
        comprobarLelagidad(binding.txtVwFutureLegalitie, carta.getLegalities()[1]);
        // Historic
        comprobarLelagidad(binding.txtVwHistoricLegalitie, carta.getLegalities()[2]);
        // Gladiator
        comprobarLelagidad(binding.txtVwGladiatorLegalitie, carta.getLegalities()[3]);
        // Pionner
        comprobarLelagidad(binding.txtVwPioneerLegalitie, carta.getLegalities()[4]);
        // Modern
        comprobarLelagidad(binding.txtVwModernLegalitie, carta.getLegalities()[5]);
        // Legacy
        comprobarLelagidad(binding.txtVwLegacyLegalitie, carta.getLegalities()[6]);
        // Pauper
        comprobarLelagidad(binding.txtVwPauperLegalitie, carta.getLegalities()[7]);
        // Vintage
        comprobarLelagidad(binding.txtVwVintageLegalitie, carta.getLegalities()[8]);

        // LINE - 2
        // Penny
        comprobarLelagidad(binding.txtVwPennyLegalitie, carta.getLegalities()[9]);
        // Commander
        comprobarLelagidad(binding.txtVwCommanderLegalitie, carta.getLegalities()[10]);
        // Brawl
        comprobarLelagidad(binding.txtVwBrawlLegalitie, carta.getLegalities()[11]);
        // Historic Brawls
        comprobarLelagidad(binding.txtVwHistoricBrawlLegalitie, carta.getLegalities()[12]);
        // Pauper Commander
        comprobarLelagidad(binding.txtVwPauperCommanderLegalitie, carta.getLegalities()[13]);
        // Duel
        comprobarLelagidad(binding.txtVwDuelLegalitie, carta.getLegalities()[14]);
        // Old School
        comprobarLelagidad(binding.txtVwOldSchoolLegalitie, carta.getLegalities()[15]);
        // Premodern
        comprobarLelagidad(binding.txtVwPremodernLegalitie, carta.getLegalities()[16]);

    }



    // Cambia el Color del texto según la Rareza
    private void comprobarRareza(String rarity, TextView txtVwRarityDetails) {
        String upperRarity = upperCaseFirst(rarity);
        txtVwRarityDetails.setText(upperRarity);
        if (rarity.equals("common")){
            txtVwRarityDetails.setTextColor(Color.rgb(0,0,0));
        }
        else if (rarity.equals("uncommon")){
            txtVwRarityDetails.setTextColor(Color.rgb(194,194,191));
        }
        else if (rarity.equals("rare")){
            txtVwRarityDetails.setTextColor(Color.rgb(253,253,44));
        }
        else if (rarity.equals("mythic")){
            txtVwRarityDetails.setTextColor(Color.rgb(255,0,0));
        }
    }

    // Pone la Primera letra en Mayúscula
    private static String upperCaseFirst(String val) {
        char[] arr = val.toCharArray();
        arr[0] = Character.toUpperCase(arr[0]);
        return new String(arr);
    }

    private void comprobarLelagidad(TextView txtVwStandardLegalitie, String legalitie) {
        String auxText = "";

        // legal
        if (legalitie.equals("legal")){
            auxText = "LEGAL";
            txtVwStandardLegalitie.setBackgroundColor(Color.rgb(57, 255, 4));
            txtVwStandardLegalitie.setTextColor(Color.rgb(0,0,0));
        }
        // not_legal
        else if (legalitie.equals("not_legal")){
            auxText = "NOT LEGAL";
            txtVwStandardLegalitie.setBackgroundColor(Color.rgb(181, 181, 181));
            txtVwStandardLegalitie.setTextColor(Color.rgb(0,0,0));

        }
        // banned
        else if (legalitie.equals("banned")){
            auxText = "BANNED";
            txtVwStandardLegalitie.setBackgroundColor(Color.rgb(255, 0, 0));

        }
        // restricted
        else if (legalitie.equals("restricted")){
            txtVwStandardLegalitie.setBackgroundColor(Color.rgb(255, 158, 12));
            auxText = "RESTRICT";
        }
        txtVwStandardLegalitie.setText(auxText);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(DetailsViewModel.class);
        // TODO: Use the ViewModel
    }

}