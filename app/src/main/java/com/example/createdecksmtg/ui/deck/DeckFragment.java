package com.example.createdecksmtg.ui.deck;

import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.createdecksmtg.MainActivity;
import com.example.createdecksmtg.R;
import com.example.createdecksmtg.databinding.DeckFragmentBinding;
import com.example.createdecksmtg.recursos.Deck;
import com.example.createdecksmtg.recursos.adapters.DecksListAdapter;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Date;

public class DeckFragment extends Fragment {

    private DeckViewModel mViewModel;

    public static ListView lsVw_decksList;
    public ImageView imgVw_returnArrowDecks;
    public ImageView imgVw_searchButtonDecks;
    public Button btn_newDeck;
    public TextInputEditText txtInpEd_searchDecks;

    public static ArrayList<Deck> allDecks = new ArrayList<>();

    public static Deck actualDeck;

    public DeckFragmentBinding binding;

    public View.OnClickListener listenerDeck;

    public static DeckFragment newInstance() {
        return new DeckFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.deck_fragment, container, false);

        binding = DeckFragmentBinding.bind(view);

        listenerDeck = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){

                    // BOTON AÑADIR CARTA
                    case R.id.btn_newDeck:
                        popUpNewDeck(view);
                        break;

                    // VOLVER AL MAIN
                    case R.id.imgVw_returnArrowDecks:
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        startActivity(intent);
                        break;

                    // BOTÓN DE BÚSQUEDA
                    case R.id.imgVw_searchButtonDecks:

                        break;
                }
            }
        };


        findView(view);
        getLiseners(listenerDeck);
        refreshDecks(getContext());

        return view;
    }

    private void popUpNewDeck(View view) {
        final View popNewDeck = getLayoutInflater().inflate(R.layout.popup_name_new_deck, null);

        Button btn_cancelNewDeck = popNewDeck.findViewById(R.id.btn_cancelNewDeck);
        TextInputEditText txtInpEd_newDeck = popNewDeck.findViewById(R.id.txtInpEd_newDeck);
        TextInputLayout txtInpLay_newDeck = popNewDeck.findViewById(R.id.txtInpLay_newDeck);
        ImageButton imgBtn_newDeck = popNewDeck.findViewById(R.id.imgBtn_newDeck);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(view.getContext());

        // PUP UP DIALOG BUILDER
        dialogBuilder.setView(popNewDeck);
        AlertDialog dialog = dialogBuilder.show();
        dialog.show();

        imgBtn_newDeck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nombreDeck = txtInpLay_newDeck.getEditText().getText().toString().trim();
                Log.d("Deck_name", nombreDeck);
                if (!nombreDeck.isEmpty()){
                    allDecks.add(new Deck(nombreDeck,new Date(System.currentTimeMillis()),"https://i.pinimg.com/originals/de/27/ba/de27ba2f2c336d4fce804bfa5ec9f3ed.jpg"));
                }
                Log.d("all_Decks", allDecks.toString());
                dialog.dismiss();
                refreshDecks(getContext());
            }
        });

        btn_cancelNewDeck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void findView(View view) {

        // List View
        lsVw_decksList = view.findViewById(R.id.lsVw_decksList);

        // Image View
        imgVw_returnArrowDecks = view.findViewById(R.id.imgVw_returnArrowDecks);
        imgVw_searchButtonDecks = view.findViewById(R.id.imgVw_searchButtonDecks);

        // Button
        btn_newDeck = view.findViewById(R.id.btn_newDeck);

        // Text Input
        txtInpEd_searchDecks = view.findViewById(R.id.txtInpEd_searchDecks);
    }

    private void getLiseners(View.OnClickListener listener) {

        // Image View
        imgVw_returnArrowDecks.setOnClickListener(listener);
        imgVw_searchButtonDecks.setOnClickListener(listener);

        // Button
        btn_newDeck.setOnClickListener(listener);

        // Text Input
        txtInpEd_searchDecks.setOnClickListener(listener);
    }

    public static void refreshDecks(Context context){
        DecksListAdapter deckAdapter = new DecksListAdapter(
                context,
                R.layout.lv_each_deck,
                allDecks
        );
        lsVw_decksList.setAdapter(deckAdapter);
    }

    public static void updateDeck(Deck deck){
        for (int i = 0; i < allDecks.size(); i++) {
            if (allDecks.get(i).getCreated_at().equals(deck.getCreated_at())){
                allDecks.set(i, deck);
            }
        }
    }

    public static void setAllDecks(ArrayList<Deck> allDecks) {
        DeckFragment.allDecks = allDecks;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(DeckViewModel.class);
        // TODO: Use the ViewModel
    }

}