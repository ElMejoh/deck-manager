package com.example.createdecksmtg.ui.inventory;

import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.createdecksmtg.MainActivity;
import com.example.createdecksmtg.R;
import com.example.createdecksmtg.recursos.Carta;
import com.example.createdecksmtg.recursos.adapters.AddCardAdapter;
import com.example.createdecksmtg.recursos.adapters.InventoryCartasAdapter;
import com.example.createdecksmtg.ui.main.MainFragment;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;

public class InventoryFragment extends Fragment {

    private InventoryViewModel mViewModel;

    public static InventoryFragment newInstance() {
        return new InventoryFragment();
    }


    // CARTAS
    public static ArrayList<Carta> allCartas;
    public static ArrayList<Carta> cartasInventory = new ArrayList<>();

    // ADAPTERS
    public InventoryCartasAdapter cartasAdapterInventory;
    public AddCardAdapter cartasAdapterAdd;

    // Elements Layout
    public static ListView lsVw_inventoryList;
    public ImageView imgVw_returnArrowInventory;
    public ImageView imgVw_searchButtonInventory;
    public Button btn_addCard;
    public TextInputEditText txtInpEd_searchInventory;
    public ImageView imgVw_eliminateCardInventory;

    // PopUp Add Card
    public AlertDialog.Builder dialogBuilder;
    public AlertDialog dialog;
    public Button btn_cancelAdd;
    public ImageView imgVw_searchAddCard;
    public TextInputEditText txtInpEd_searchCardsAdd;
    public ListView lsVw_cartasAdd;
    public TextView txtVw_nameCardAdd;

    public boolean first = true;



    public View.OnClickListener listenerInventory;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inventory_fragment, container, false);


        listenerInventory = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){

                    // Boton -  AÑADIR CARTA
                    case R.id.btn_addCard:
                        createAddCardsPupUp();
                        break;

                    // ImageView - VOLVER AL MAIN
                    case R.id.imgVw_returnArrowInventory:
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        startActivity(intent);
                        break;

                    // ImageView - BOTÓN DE BÚSQUEDA
                    case R.id.imgVw_searchButtonInventory:

                        break;
                }
                refresh();
            }
        };

        refresh();
        findView(view);

        return view;
    }

    // CREA UN POP UP / DIALOG ALERT
    private void createAddCardsPupUp(){
        dialogBuilder = new AlertDialog.Builder(this.getContext());

        final View contactPopUpView = getLayoutInflater().inflate(R.layout.popup_add_card, null);
        final View lvCardAdd = getLayoutInflater().inflate(R.layout.lv_card_add, null);

        // CONTACT POP UP
        lsVw_cartasAdd = contactPopUpView.findViewById(R.id.lsVw_cartasAdd);
        imgVw_searchAddCard = contactPopUpView.findViewById(R.id.imgVw_searchAddCard);
        btn_cancelAdd = contactPopUpView.findViewById(R.id.btn_cancelAdd);
        txtInpEd_searchCardsAdd = contactPopUpView.findViewById(R.id.txtInpEd_searchCardsAdd);

        // LV CARD ADD
        txtVw_nameCardAdd = lvCardAdd.findViewById(R.id.txtVw_nameCardAdd);
        lsVw_cartasAdd.setAdapter(cartasAdapterAdd);

        // PUP UP DIALOG BUILDER
        dialogBuilder.setView(contactPopUpView);
        dialog = dialogBuilder.show();
        dialog.show();

        btn_cancelAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                refresh();
            }
        });

    }


    // ENCUENTRA TODAS LAS VIEW
    public void findView(View view) {

        // List View
        lsVw_inventoryList = view.findViewById(R.id.lsVw_inventoryList);

        // Image View
        imgVw_returnArrowInventory = view.findViewById(R.id.imgVw_returnArrowInventory);
        imgVw_searchButtonInventory = view.findViewById(R.id.imgVw_searchButtonInventory);

        // Button
        btn_addCard = view.findViewById(R.id.btn_addCard);

        // Text Input
        txtInpEd_searchInventory = view.findViewById(R.id.txtInpEd_searchInventory);

        getLiseners(listenerInventory);

    }



    private void getLiseners(View.OnClickListener listener) {

        // Image View
        imgVw_returnArrowInventory.setOnClickListener(listener);
        imgVw_searchButtonInventory.setOnClickListener(listener);

        // Button
        btn_addCard.setOnClickListener(listener);

    }

    public void refresh() {
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    public static void refreshInventory(Context context){
        InventoryCartasAdapter cartasAdapterInventory = new InventoryCartasAdapter(
                context,
                R.layout.lv_card_inventory,
                cartasInventory,
                "Delete",
                null
        );
        lsVw_inventoryList.setAdapter(cartasAdapterInventory);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(InventoryViewModel.class);
        // TODO: Use the ViewModel
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            allCartas = MainFragment.getAllCards();
            cartasAdapterAdd = new AddCardAdapter(
                    getContext(),
                    R.layout.lv_card_add,
                    allCartas
            );
            cartasAdapterInventory = new InventoryCartasAdapter(
                    getContext(),
                    R.layout.lv_card_inventory,
                    cartasInventory,
                    "Delete",
                    null
            );
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            lsVw_inventoryList.setAdapter(cartasAdapterInventory);
        }
    }

    public static void addCartaInventory(Carta carta){
        cartasInventory.add(carta);
    }

    public static ArrayList<Carta> getCartasInventory(){
        return cartasInventory;
    }




}