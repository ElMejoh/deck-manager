package com.example.createdecksmtg.ui.main;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.manager.TargetTracker;
import com.bumptech.glide.request.RequestOptions;
import com.example.createdecksmtg.DeckActivity;
import com.example.createdecksmtg.InventoryActivity;
import com.example.createdecksmtg.R;
import com.example.createdecksmtg.recursos.Carta;
import com.example.createdecksmtg.recursos.cartaAPI;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Target;
import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;


public class MainFragment extends Fragment {

    private MainViewModel mViewModel;

    public static MainFragment newInstance() {
        return new MainFragment();
    }


    public ImageView imgV_cardDecks;
    public ImageView imgV_cardInventory;

    public ConstraintLayout cl_cardDecks;
    public ConstraintLayout cl_cardInventory;

    public static ArrayList<Carta> allCards;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);

        imgV_cardDecks = view.findViewById(R.id.imgV_cardDecks);
        imgV_cardInventory = view.findViewById(R.id.imgV_cardInventory);

        liseners(view);

        cl_cardDecks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), DeckActivity.class);
                startActivity(intent);
            }
        });

        cl_cardInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), InventoryActivity.class);
                startActivity(intent);
            }
        });

        String urlDeck = "https://assets.moxfield.net/cards/card-Lmd63-art_crop.jpg";
        String urlInventory = "https://assets.moxfield.net/cards/card-EgDjp-art_crop.webp";

        Glide.with(this.getContext())
                .load(urlDeck)
                .into(imgV_cardDecks);
        Glide.with(this.getContext())
                .load(urlInventory)
                .into(imgV_cardInventory);

        refresh();

        return view;
    }

    private void refresh() {
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    private void liseners(View view) {
        cl_cardDecks = view.findViewById(R.id.cl_cardDecks);
        cl_cardInventory = view.findViewById(R.id.cl_cardInventory);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Carta>> {
        @Override
        protected ArrayList<Carta> doInBackground(Void... voids) {
            if (allCards == null){
                allCards = new cartaAPI().getCartes();
            }
            return allCards;
        }

        @Override
        protected void onPostExecute(ArrayList<Carta> cartas) {
            allCards = cartas;
        }
    }

    public static ArrayList<Carta> getAllCards() {
        return allCards;
    }
}