package com.example.createdecksmtg;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.createdecksmtg.ui.inventory.InventoryFragment;

public class InventoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inventory_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, InventoryFragment.newInstance())
                    .commitNow();
        }
    }
}