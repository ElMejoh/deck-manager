package com.example.createdecksmtg;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.createdecksmtg.ui.insidedeck.InsideDeckFragment;

public class InsideDeckActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inside_deck_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, InsideDeckFragment.newInstance())
                    .commitNow();
        }
    }
}