package com.example.createdecksmtg;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.createdecksmtg.ui.deck.DeckFragment;

public class DeckActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deck_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, DeckFragment.newInstance())
                    .commitNow();
        }
    }
}